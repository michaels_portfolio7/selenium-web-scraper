import shutil
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import os
import glob
import pandas as pd
import clipboard
import time

class Driver:
    def __init__(self):
        self.generateBroswer()
        self.authForm()
        self.findReport()
        # self.downloadReport()
        # self.closeDriver()
        # self.cleanFolder()

        #####################
        # Configures Browser
        #####################

    def generateBroswer(self):
        profile = webdriver.FirefoxProfile()

        # This is to over-ride default Firefox settings, for download
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', os.getcwd())
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk',
                               'text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')

        ### ******* CHANGE $PATH TO YOUR GECKO DRIVER HERE ******* ###
        self.browser = webdriver.Firefox(firefox_profile=profile,
                                         executable_path=os.path.join(os.getcwd(), "geckodriver"))

        #####################
        # Auth Login Form
        #####################

    def authForm(self):
        # Navigate
        self.browser.get('https://www.rystadenergy.com/clients/')
        self.browser.implicitly_wait(30)

        # Authentication
        self.browser.find_element_by_name("Username").send_keys("")  # Needs ENV Var
        self.browser.find_element_by_id("Password").send_keys("")  # Needs ENV Var
        self.browser.find_element_by_name("button").click()

        ####################################################
        # Finds report & can also make new reports on demand,
        # by "saving report" on dashboard and populating
        # new url ID from inspect.
        #####################################################

    def findReport(self):
        self.browser.get(
            'https://www.rystadenergy.com/clients/cube-dashboards/workflow/?did=11&bid=4775933:B5764F3B-B644-476D-9717-08B167590856')
        self.browser.implicitly_wait(60)

        # The Holy Line
        iframe = self.browser.find_element_by_xpath("/html/body/div[1]/div/main/div[2]/iframe")
        self.browser.switch_to.frame(iframe)

        ####################################################
        # FIRST DATE FIELD
        #####################################################
        self.browser.find_element_by_xpath(
            "/html/body/div[2]/root/div/div/div[1]/div/div/div/exploration-container/exploration-container-modern/div/div/div/exploration-host/div/div/exploration/div/explore-canvas-modern/div/div[2]/div/div[2]/div[2]/visual-container-repeat/visual-container-modern[6]/transform/div/div[3]/div/visual-modern/div/div/div[2]/div/div[1]/div/div[1]").click()
        self.browser.find_element_by_xpath(
            "/html/body/div[2]/root/div/div/div[1]/div/div/div/exploration-container/exploration-container-modern/div/div/div/exploration-host/div/div/exploration/div/explore-canvas-modern/div/div[2]/div/div[2]/div[2]/visual-container-repeat/visual-container-modern[6]/transform/div/div[3]/div/visual-modern/div/div/div[2]/div/div[1]/div/div[1]").click()

        # create action chain object
        action = ActionChains(self.browser)

        clipboard.copy(report_startdate)

        # perform the operation
        action.key_down(Keys.COMMAND).send_keys('A').perform()
        action.key_down(Keys.COMMAND).send_keys('V').perform()

        ####################################################
        # SECOND DATE FIELD
        #####################################################
        self.browser.find_element_by_xpath(
            "/html/body/div[2]/root/div/div/div[1]/div/div/div/exploration-container/exploration-container-modern/div/div/div/exploration-host/div/div/exploration/div/explore-canvas-modern/div/div[2]/div/div[2]/div[2]/visual-container-repeat/visual-container-modern[6]/transform/div/div[3]/div/visual-modern/div/div/div[2]/div/div[1]/div/div[2]").click()
        self.browser.find_element_by_xpath(
            "/html/body/div[2]/root/div/div/div[1]/div/div/div/exploration-container/exploration-container-modern/div/div/div/exploration-host/div/div/exploration/div/explore-canvas-modern/div/div[2]/div/div[2]/div[2]/visual-container-repeat/visual-container-modern[6]/transform/div/div[3]/div/visual-modern/div/div/div[2]/div/div[1]/div/div[2]").click()

        # # create action chain object
        # action = ActionChains(self.browser)


        clipboard.copy(report_enddate)

        # perform the operation
        action.key_down(Keys.COMMAND).send_keys('A').perform()
        action.key_down(Keys.COMMAND).send_keys('V').perform()

        ####################################################
        # BASIN 
        #####################################################
        
        #Try to reload iframe
        self.browser.switch_to.default_content()
        self.browser.switch_to.frame(iframe)

        self.browser.find_element_by_xpath("/html/body/div[2]/root/div/div/div[1]/div/div/div/exploration-container/exploration-container-modern/div/div/div/exploration-host/div/div/exploration/div/explore-canvas-modern/div/div[2]/outspace-pane/article/div[2]/filter-pane-modern/section/div/div/section[2]/div[2]/div/filter[1]/div/div[1]/div[1]/div/pbi-text-label/div").click()
        # self.browser.find_element_by_xpath(
        #     "/html/body/div[2]/root/div/div/div[1]/div/div/div/exploration-container/exploration-container-modern/div/div/div/exploration-host/div/div/exploration/div/explore-canvas-modern/div/div[2]/outspace-pane/article/div[2]/filter-pane-modern/section/div/div/section[2]/div[2]/div/filter[1]/div/div[1]/div[1]/div/pbi-text-label/div").click()
        #
        # self.browser.find_element_by_xpath(
        #     "/html/body/div[2]/root/div/div/div[1]/div/div/div/exploration-container/exploration-container-modern/div/div/div/exploration-host/div/div/exploration/div/explore-canvas-modern/div/div[2]/outspace-pane/article/div[2]/filter-pane-modern/section/div/div/section[2]/div[2]/div/filter[1]/div/div[2]/div/filter-visual/div/visual-modern/div/div/div[2]/div/div[2]/div/div/div/div[1]/div/div").click()


        ####################################################
        # Waits 60 seconds to close session as the site and
        # report generation can sometimes be slow.
        #####################################################

    def downloadReport(self):
        # Wait for report to completely load, this may take time depending on size
        time.sleep(15)
        self.browser.switch_to.default_content()
        self.browser.find_element_by_link_text("Download Data").click()

    def closeDriver(self):
        # Waits for download to start
        self.browser.implicitly_wait(20)
        # Wait for report to report to download, this may take time depending on size
        time.sleep(120)
        self.browser.quit()

    def cleanFolder(self):
        sourcepath = os.getcwd()
        sourcefiles = os.listdir(sourcepath)
        destinationpath = os.path.join(sourcepath, "files/")
        for file in sourcefiles:
            if file.endswith('.xlsx'):
                shutil.move(os.path.join(sourcepath, file), os.path.join(destinationpath, file))

if __name__ == '__main__':
    #### CONFIGURE DATES FOR REPORT
    report_startdate = "1/1/2019"
    report_enddate = "2/17/2019"

    #### STARTS DRIVER
    Driver()

    #### PANDAS
    list_of_files = glob.glob(r'files/*.xlsx') # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)
    df = pd.read_excel (latest_file)
    print (df)
